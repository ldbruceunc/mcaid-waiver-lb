/*working*/
*libname b "/n2/projects/medicaid_testsample/Data/";
libname c "/local/master/NCDMA_SVC_Master/";
libname m "/n2/projects/Medicaid_Waiver_Eval/analytics/users/lbruce/";
libname s "/n2/projects/Medicaid_Waiver_Eval/temp_space/";
libname full "/nearline/data/master/NCDMA_SVC_Master/";
libname cls "/n2/projects/Medicaid_Waiver_Eval/analytics/metrics/codelists/";
libname der "/n2/projects/Medicaid_Waiver_Eval/Derived/sasdata/";
libname test "/n2/projects/medicaid_testsample/Data/"; 
libname full "/nearline/data/master/NCDMA_SVC_Master/"; 
libname moloci "/n2/projects/Medicaid_Waiver_Eval/analytics/users/moloci/"; 
libname bdat "/n2/projects/Medicaid_Waiver_Eval/analytics/users/lbruce/data/"; 

*Include codelists for M3 specified in the excel document Value Set Programs.xlsx in the folder:
https://adminliveunc.sharepoint.com/sites/1115waivereval/Shared Documents/Quantitative/Metrics/;
%include "/n2/projects/Medicaid_Waiver_Eval/analytics/users/lbruce/M3/codelists_m3.sas"; 


/*Goals: Number of unique beneficiaries enrolled in measurement period who receive MAT
or have qualifying facility, provider, or rx claims with a SUD diagnosis and SUD-related tx service
during measurement period or 11 months prior */

data rx1;
	set full.rx_claims_svcyr_2020 
		 (where = ((DRUG_CD in (&AUD_RX., &OUD_RX.) ) and final = 'Y') 
		 keep=MID lne_svc_bgn_dt DRUG_CD DRUG_NM DRUG_GNRC_NM DRUG_RX_DT CLM_HDR_PD_DT TRNSCT_CNTL_NBR FINAL  ); 
		 run; 
*obs=10000;
proc sort data=rx1 nodupkey out=bdat.rx1;
	by MID lne_svc_bgn_dt;
	run;
proc sql;
create table rxc as
select INTNX('month',LNE_SVC_BGN_DT,6) as ServiceMonth format = date9.,  count (distinct MID) as UMID
from bdat.rx1
group by 1
order by 1;
/*14,527*/



data pr1;
	set full.PR_claims_svcyr_2020  
		 (where = ((PROC_ADJDC_CD in (&AOD_MED_PROC., 		&DETOX_PROC., 	&ED_PROC., 	&RESTX_PROC., 	
									 &IET_VISITS_PROC_GP1., &IET_VISITS_PROC_GP2., 	&IET_ST_AL_PROC., 	
									 &TEL_PROC., 			&ONLN_PROC., 	&MB_PREG_PROC.))
		 or (POS_CD in (&IET_VISITS_POS_GP1.)) and final = 'Y')
		 keep=MID lne_svc_bgn_dt DIAG_CD_01-DIAG_CD_10 POS_CD PROC_ADJDC_CD PROC_MOD_1_DESC CLM_HDR_PD_DT final TRNSCT_CNTL_NBR ); /*obs=10000);*/ 
	array dx DIAG_CD_01-DIAG_CD_10;
    do over dx;
     if dx in: (&AUD_DIAG_SET1., &ODAD_DIAG_SET3., &OUD_DIAG_SET2.) then do;
       sud_dx=1; leave;
     end;
    end;
		 run; 

proc sort data=pr1 nodupkey out=bdat.pr1;
	by MID lne_svc_bgn_dt;
	run;

data pr2;
	set pr1 keep = ;
if  PROC_ADJDC_CD in (&AOD_MED_PROC., 		&DETOX_PROC., 	&ED_PROC., 	&RESTX_PROC., 	
									 &IET_VISITS_PROC_GP1., &IET_VISITS_PROC_GP2., 	&IET_ST_AL_PROC.) then 

		 (where = ((PROC_ADJDC_CD in (&AOD_MED_PROC., 		&DETOX_PROC., 	&ED_PROC., 	&RESTX_PROC., 	
									 &IET_VISITS_PROC_GP1., &IET_VISITS_PROC_GP2., 	&IET_ST_AL_PROC., 	
									 &TEL_PROC., 			&ONLN_PROC., 	&MB_PREG_PROC.))
		 or (POS_CD in (&IET_VISITS_POS_GP1.)) and final = 'Y')
		 keep=MID lne_svc_bgn_dt DIAG_CD_01-DIAG_CD_10 POS_CD PROC_ADJDC_CD PROC_MOD_1_DESC CLM_HDR_PD_DT final TRNSCT_CNTL_NBR ); /*obs=10000);*/ 
		  array dx DIAG_CD_01-DIAG_CD_10;
    do over dx;
     if dx in: (&AUD_DIAG_SET1., &ODAD_DIAG_SET3., &OUD_DIAG_SET2.) then do;
       sud_dx=1; leave;
     end;
    end;
		 run; 

proc freq data= bdat.pr1 ;
tables sud_dx;
run;

/* Step 1 (part 1 of 2)*/
proc sql;
create table step1_1 as
select distinct INTNX('month',DRUG_RX_DT,6) as ServiceMonth format = date9., MID, 1 as RX1_flag /*TRNSCT_CNTL_NBR*/
from bdat.rx1
group by 1,2
order by 1;

/* Step 1 (part 2 of 2)*/
proc sql;
create table step1_2 as
select distinct INTNX('month',DRUG_RX_DT,6) as ServiceMonth format = date9., MID, 1 as RX1_flag /*TRNSCT_CNTL_NBR*/
from bdat.rx1
group by 1,2
order by 1;



AUD_DIAG_SET1.sas
ODAD_DIAG_SET3.sas
OUD_DIAG_SET2.sas

proc sql OUTOBS=10000;
create table pr1 as
select MID, TRNSCT_CNTL_NBR, lne_svc_bgn_dt, POS_CD, &DXCODES., PROC_ADJDC_CD, PROC_MOD_1_DESC, CLM_HDR_PD_DT
from full.PR_claims_svcyr_2020
where PROC_ADJDC_CD in (&AOD_MED_PROC., &DETOX_PROC., 	&ED_PROC., 	&RESTX_PROC., 	
									 &IET_VISITS_PROC_GP1., &IET_VISITS_PROC_GP2., 	&IET_ST_AL_PROC., 	
									 &TEL_PROC., 			&ONLN_PROC., 	&MB_PREG_PROC.)
									 OR (POS_CD in (&IET_VISITS_POS_GP1.))
									 OR (  in (&AUD_DIAG_SET1.))
									 ;
/*%LET DXCODES = 'DIAG_CD_01','DIAG_CD_02','DIAG_CD_03','DIAG_CD_04','DIAG_CD_05',
			   'DIAG_CD_06','DIAG_CD_07','DIAG_CD_08','DIAG_CD_09','DIAG_CD_10;			*/						

group by year, month
order by year, month;


*libname b "/local/projects/medicaid_testsample/Data/";
*data source /n2/projects/Medicaid_Waiver_Eval/Derived/sasdata/eligibles_dec2020 ;

proc contents data =  "/n2/projects/Medicaid_Waiver_Eval/Derived/sasdata/eligibles_dec2020";
run;

data eligibles_dec2020;
set der.eligibles_dec2020;
run;

*Match up the denominators from Lily;

proc sql;
create table rpt2020 as
select distinct YEAR_MON, count(distinct (MID)) as DistinctMID
from eligibles_dec2020
where full_elig = 1
group by year, month
order by year, month;











data test; 
	set full.PR_CLAIMS_SVCYR_2020 
		(where = (PROC_ADJDC_CD in (&AOD_PROC.)) 
		 keep=lne_svc_bgn_dt MID PROC_ADJDC_CD obs=10000); 
*rename to HCPCS;
run; 



/**** 
Test using include options 
*******/ 
options mlogic mprint symbolgen fullstimer compress=yes;  
%put Job Started Execution at %sysfunc(time(),timeampm.) on %sysfunc(date(),worddate.).; 

/* start with 5% sample */ 


 */local/users/liana/projects/Medicaid_Waiver_Eval/analytics/users/massing/Code/M3/M3_AOD_MAT_PROC.sas;

/**** 
Just so you can see what the file looks like: 

%LET AOD_PROC='H0020', 
'H0033', 
'J0571', 
'J0572', 
'J0573', 
'J0574', 
'J0575', 
'J2315', 
'S0109'; 

***/ 

%include m3aodmat; 

  

data test; 
	set full.PR_CLAIMS_SVCYR_2020 
		(where = (PROC_ADJDC_CD in (&AOD_PROC.)) 
		 keep=lne_svc_bgn_dt MID PROC_ADJDC_CD obs=10000); 
*rename to HCPCS;
run;  


/*useful items from Lily
template file:
/local/users/liana/projects/Medicaid_Waiver_Eval/analytics/metrics/M3/grid8pt.sas

macro for pulling in all value sets for m3:
/local/users/liana/projects/Medicaid_Waiver_Eval/analytics/metrics/M3/01_m3from201609.sas
/local/users/liana/projects/Medicaid_Waiver_Eval/analytics/metrics/macros/valuesets.sas
*/

data m.aod_mat2020;
set test;
run;

  
  


proc sql;
create table rpt2020 as
select  count(distinct (MID))
from test;


where flag = 1 and 
group by RPT_PERIOD
order by RPT_PERIOD;

  
distinct  MBR_REF_ELGB_BGN_DT,
  

/* 

You can do a lot more with this, like insert external macros within your program. We might want to look at doing that for metrics that are running on similar  

numerators/denomiators. This way we can run just one metric, yet update the common numerator/denominator. However, I think this is far away from today.  

  

*/ 






